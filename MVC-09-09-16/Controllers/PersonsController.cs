﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_09_09_16.Models;
using PagedList;

namespace MVC_09_09_16.Controllers
{
    public class PersonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index2()
        {
            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name");
            return View();
        }

        public JsonResult getPerson()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Person> people = db.Persons.ToList();
            return Json(people, JsonRequestBehavior.AllowGet);
        }
        // GET: Personas
        public ActionResult Index(string sort ,string search, int? page)
        {
            ViewBag.NameSort = sort == "Name" ? "Name_desc" : "Name";
            ViewBag.LastNameSort = sort == "LastName" ? "LastName_desc" : "LastName";
            ViewBag.TypeIdentificationSort = sort == "TypeIdentification" ? "TypeIdentification_desc" : "TypeIdentification";
            ViewBag.IdentificationSort = sort == "Identification" ? "Identification_desc" : "Identification";

            ViewBag.CurrentSort = sort;
            ViewBag.CurrentSearch = search;

            IQueryable<Person> personae = db.Persons;

            if (!String.IsNullOrEmpty(search))
                personae = personae.Where(pp => pp.Name.Contains(search) || pp.Identification.Contains(search) || pp.TypeIdentifications.Name.Contains(search));
            switch (sort)
            {
                case "Name":
                    personae = personae.OrderBy(p => p.Name).ThenBy(p => p.LastName);
                    break;
                case "Name_desc":
                    personae = personae.OrderByDescending(p => p.Name);
                    break;
                case "LastName":
                    personae = personae.OrderBy(p => p.LastName);
                    break;
                case "LastName_desc":
                    personae = personae.OrderByDescending(p => p.LastName);
                    break;
                case "TypeIdentification":
                    personae = personae.OrderBy(p => p.TypeIdentifications.Name);
                    break;
                case "TypeIdentification_desc":
                    personae = personae.OrderByDescending(p => p.TypeIdentifications.Name);
                    break;
                case "Identification":
                    personae = personae.OrderBy(p => p.Identification);
                    break;
                case "Identification_desc":
                    personae = personae.OrderByDescending(p => p.Identification);
                    break;
                default:
                    personae = personae.OrderBy(p => p.Name);
                    break;
            }

            int pageSize = 3;
            int pageNumber = page ?? 1;
            return View(personae.ToPagedList(pageNumber, pageSize));
        }

        // GET: Personas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person persona = await db.Persons.FindAsync(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name");
            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,LastName,TypeIdentificationId,Identification,Active")] Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Persons.Add(person);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                TempData["MessageToClient"] = String.Format("DATOS DUPLICADOS DEL REGISTRO A GUARDAR!");
                                break;
                            default:
                                throw;
                        }
                }
            }

            return View(person);
        }

        // GET: Personas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person persona = await db.Persons.FindAsync(id);

            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name", persona.TypeIdentificationId);

            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,LastName,TypeIdentificationId,Identification")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: Personas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person persona = await db.Persons.FindAsync(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Person persona = await db.Persons.FindAsync(id);
            db.Persons.Remove(persona);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
