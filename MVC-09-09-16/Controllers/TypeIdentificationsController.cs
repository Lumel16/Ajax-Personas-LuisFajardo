﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_09_09_16.Models;

namespace MVC_09_09_16.Controllers
{
    public class TypeIdentificationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TypeIdentifications
        public async Task<ActionResult> Index()
        {
            return View(await db.TypeIdentifications.ToListAsync());
        }

        // GET: TypeIdentifications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeIdentification typeIdentification = await db.TypeIdentifications.FindAsync(id);
            if (typeIdentification == null)
            {
                return HttpNotFound();
            }
            return View(typeIdentification);
        }

        // GET: TypeIdentifications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeIdentifications/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,Active")] TypeIdentification typeIdentification)
        {
            if (ModelState.IsValid)
            {
                db.TypeIdentifications.Add(typeIdentification);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(typeIdentification);
        }

        // GET: TypeIdentifications/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeIdentification typeIdentification = await db.TypeIdentifications.FindAsync(id);
            if (typeIdentification == null)
            {
                return HttpNotFound();
            }
            return View(typeIdentification);
        }

        // POST: TypeIdentifications/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,Active")] TypeIdentification typeIdentification)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeIdentification).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(typeIdentification);
        }

        // GET: TypeIdentifications/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeIdentification typeIdentification = await db.TypeIdentifications.FindAsync(id);
            if (typeIdentification == null)
            {
                return HttpNotFound();
            }
            return View(typeIdentification);
        }

        // POST: TypeIdentifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TypeIdentification typeIdentification = await db.TypeIdentifications.FindAsync(id);
            db.TypeIdentifications.Remove(typeIdentification);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
