﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_09_09_16.Models
{
    [Table("person", Schema = "adm")]
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_ADM_PERSON", 1, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre")]
        [StringLength(30, ErrorMessage = "El nombre no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Index("INDEX_ADM_PERSON", 2, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el apellido")]
        [StringLength(30, ErrorMessage = "El apellido no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        public int TypeIdentificationId { get; set; }

        [Index("INDEX_ADM_PERSON_IDENTIFICATION", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el numero de identificacion")]
        [StringLength(20, ErrorMessage = "La identificacion no debe ser mayor de 20 caracteres.")]
        [Display(Name = "Identificacion")]
        public string Identification { get; set; }

        public bool Active { get; set; }

        public virtual TypeIdentification TypeIdentifications { get; set; }
    }
}