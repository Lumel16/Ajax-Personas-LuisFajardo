﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_09_09_16.Models
{
    [Table("student", Schema = "adm")]
    public class Student
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_ADM_STUDENT", 1, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre del estudiante")]
        [StringLength(30, ErrorMessage = "El nombre no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Index("INDEX_ADM_STUDENT", 2, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el apellido del estudiante")]
        [StringLength(30, ErrorMessage = "El apellido no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Index("INDEX_ADM_STUDENT", 3, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el numero de carnet del estudiante")]
        [StringLength(10, ErrorMessage = "El carnet no debe ser mayor de 10 caracteres.")]
        [Display(Name = "Carnet")]
        public string IdCard { get; set; }

        public string Career { get; set; }

        [Index("INDEX_ADM_STUDENT_IDENTIFICATION", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el numero de identificacion del estudiante")]
        [StringLength(20, ErrorMessage = "La identificacion no debe ser mayor de 20 caracteres.")]
        [Display(Name = "Identificacion")]
        public string Identification { get; set; }
    }
}