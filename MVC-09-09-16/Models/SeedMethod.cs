﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace MVC_09_09_16.Models
{
    public class SeedMethod
    {
        private ApplicationDbContext _ctx;

        public SeedMethod(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Seed(ApplicationDbContext context)
        {
            context.TypeIdentifications.AddOrUpdate(ti => ti.Name,
                new TypeIdentification {Name = "Cedula", Description = "Cedula", Active = true},
                new TypeIdentification {Name = "Pasaporte", Description = "Pasaporte", Active = true},
                new TypeIdentification {Name = "Carnet", Description = "Carnet", Active = true}
                );
            context.SaveChanges();

            TypeIdentification typeIdentificationCedula = context.TypeIdentifications.FirstOrDefault(ti => ti.Name == "Cedula");

            context.SaveChanges();

            context.Persons.AddOrUpdate(p => new {p.Name, p.LastName},
                new Person { Name = "Juan", LastName = "Ramirez", TypeIdentificationId = typeIdentificationCedula.Id, Identification = "001-200255-0021V", Active = true },
                new Person { Name = "Pedro", LastName = "Gonzales", TypeIdentificationId = typeIdentificationCedula.Id, Identification = "001-120596-0457J", Active = true },
                new Person { Name = "Ana", LastName = "Perez", TypeIdentificationId = typeIdentificationCedula.Id, Identification = "001-210594-2145C", Active = true }
                );
            context.SaveChanges();

        }
    }
}