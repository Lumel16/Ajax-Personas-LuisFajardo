using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC_09_09_16.Models;

namespace MVC_09_09_16.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVC_09_09_16.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MVC_09_09_16.Models.ApplicationDbContext context)
        {
            new SeedMethod(context).Seed(context);
            //var userManager =
            //    new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            //userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
            //                            {
            //                                AllowOnlyAlphanumericUserNames = false
            //                            };
            //var roleManager =
            //    new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));

            //string name = "usuario@gmail.com";
            //string password = "contraseņa";
            //string firstName = "Admin";
            //string roleName = "Admin";

            //var role = roleManager.FindByName(roleName);

            //if (role == null)
            //{
            //    role = new ApplicationRole(roleName);
            //    var roleResult = roleManager.Create(role);
            //}

            //var user = userManager.FindByName(name);

            //if (user == null)
            //{
            //    user = new ApplicationUser{UserName = name, Email = name, FirstName = firstName};
            //    var result = userManager.Create(user, password);
            //    result = userManager.SetLockoutEnabled(user.Id, false);
            //}

            //var rolesForUser = userManager.GetRoles(user.Id);

            //if (!rolesForUser.Contains(role.Name))
            //{
            //    var result = userManager.AddToRole(user.Id, role.Name);
            //}

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
