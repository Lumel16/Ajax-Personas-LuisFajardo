﻿function refresh() {
    /*Llenado de la tabla*/
    $("#dinamicTable tbody").html("");

    $.ajax({
        url: '/Persons/getPerson',
        datatype: 'JSON',
        method: 'GET'
    }).done(function (data) {
        var tabla, concat,btn1,btn2;

        $.each(data, function (i, val) {
            $.each(val, function (j, v) {
                switch(j) {
                    case 'Name': tabla += '<td id-name="' + val.Id + '">' + v + "</td>";
                        //personData[0] = v;
                        break;
                    case 'LastName': tabla += '<td id-lastname="' + val.Id + '">' + v + "</td>";
                        //personData[1] = v;
                        break;
                    case 'TypeIdentificationId': tabla += '<td id-ti="' + val.Id + '">' + v + "</td>";
                        //personData[2] = v;
                        break;
                    case 'Identification': tabla += '<td id-ident="' + val.Id + '">' + v + "</td>";
                        //personData[3] = v;
                        break;
                }
            });
            //console.log(val.Id);

            //Creando el boton editar.
            btn1 = '<a href="#" type="button" class="btnEditar btn btn-info btn-xs" id="' + val.Id + '">Editar</a>';

            //Creando el boton eliminar.
            btn2 = '<a href="#" type="button" class="btnBorrar btn btn-danger btn-xs" id="' + val.Id + '">Borrar</a>';
            
            //btn = '<a href="#" type="button" class="btnBorrar" id="100">sdfs</a>';
            concat += '<tr id-row="' + val.Id + '">' + tabla + '<td>' + btn1 + '</td>' + '<td>' + btn2 + '</td> </tr>';
            tabla = "";
            btn1 = "";
            btn2 = "";
        });

        $("#dinamicTable tbody").append(concat);
    });
}

$(window).load(function () {
    var idPerson;
    /*Refresh llena la tabla*/
    refresh();
    /*Evento Click para el boton guardar - guarda datos en la base de datos */
    $('#sendData').on('click', function (event) {
        event.preventDefault();

        $.ajax({
            url: '/Persons/Create',
            type: 'POST',
            data: $("#formPersons").serialize(),
        })
            .done(function() {
                //console.log("guardado");
                refresh();
                $("#modalAgregar").modal('toggle');
            })
            .fail(function() {
                alert("Error al guardar");
            });
    });
    
    /*Evento para capturar el id del boton editar*/
    $('tbody').on('click', 'a.btnEditar', function (event) {
        var personData = [];
        event.preventDefault();

        var id = this.id;
        idPerson = id;
        //console.log(id);
        //$('#dinamicTable tbody tr[id-row="' + idPerson + '"]')
        var row = $(this).parent().parent();
        row.children().each(function(i,v) {
            //console.log(i);
            //console.log(v.innerHTML);
            personData[i] = v.innerHTML;
        });
        //var Id = $("#formEditar input[name=Id]:hidden");
        //Id.value = idPerson;
        $("#formEditar #Id").val(idPerson);
        console.log(id);
        $("#formEditar #Name").val(personData[0]);
        $("#formEditar #LastName").val(personData[1]);
        $("#formEditar #TypeIdentificationId").val(personData[2]);
        $("#formEditar #Identification").val(personData[3]);
        $("#modalEditar").modal();

    });
    
    /*Evento Click para el boton guardar en editar registro - guarda datos actualizados en la base de datos */
    $('#btnActualizar').on('click', function (event) {
        event.preventDefault();

        $.ajax({
            url: '/Persons/Edit/' + idPerson,
            type: 'POST',
            data: $("#formEditar").serialize(),
        })
            .done(function () {
                refresh();
                $("#modalEditar").modal('toggle');
            })
            .fail(function () {
                alert("Error al actualizar los datos");
            });
    });
    
    /*Evento para capturar el id del boton borrar*/
    $('tbody').on('click', 'a.btnBorrar', function (event) {
        event.preventDefault();

        var id = this.id;
        idPerson = id;
        //console.log(id);
        //$('#deletePerson').attr('data-eliminar', id);
        $("#modalEliminar").modal();

    });
    
    /*Evento para borrar un registro*/
    $("#deletePerson").on('click', function () {
        event.preventDefault();
        //var id = $(this).attr('data-eliminar');

        //console.log("Borrando...");
        $.ajax({
            url: '/Persons/Delete/' + idPerson,
            type: 'POST',
            data: idPerson
        })
            .done(function () {
                //console.log("guardado");
                refresh();
                $("#modalEliminar").modal('toggle');
            })
            .fail(function () {
                alert("Error al eliminar");
                //console.log(idPerson);
            });
    });
});

